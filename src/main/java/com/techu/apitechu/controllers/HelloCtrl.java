package com.techu.apitechu.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloCtrl {

    @RequestMapping("/")
    public String index(){
        return "Hello World!!";
    }


    @RequestMapping("/hello")
    public String hello(@RequestParam(value="name", defaultValue = "Tech U") String name){
        return String.format("Hello %s", name);
    }

}
