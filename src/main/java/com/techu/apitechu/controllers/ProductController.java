package com.techu.apitechu.controllers;

import com.techu.apitechu.models.ProductModel;
import com.techu.apitechu.models.mocks.Mocks;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";
    static final String resourcePath = "/products";

    @GetMapping(APIBaseUrl + resourcePath)
    public ArrayList<ProductModel> getProducts() {

        System.out.println("getProducts");

        return Mocks.productModels;
    }

    @GetMapping(APIBaseUrl + resourcePath + "/{id}")
    public ProductModel getProductById(@PathVariable String id) {

        System.out.println("getProduct with id: " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product : Mocks.productModels){
            if(product.getId().equals(id)){
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + resourcePath)
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("id: " + newProduct.getId());
        System.out.println("description: " + newProduct.getDesc());
        System.out.println("price: " + newProduct.getPrice());

        Mocks.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + resourcePath + "/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel updateProduct){
        System.out.println("updateProduct");
        System.out.println("id: " + id);

        for(ProductModel product : Mocks.productModels){
            if(product.getId().equals(id)){
                product.setDesc(updateProduct.getDesc());
                product.setPrice(updateProduct.getPrice());
            }
        }

        return updateProduct;
    }

    @DeleteMapping(APIBaseUrl + resourcePath + "/{id}")
    public ProductModel removeProduct(@PathVariable String id){

        System.out.println("updateProduct");
        System.out.println("id: " + id);

        ProductModel productToRemove = null;

        for(ProductModel product : Mocks.productModels){
            if(product.getId().equals(id)){
                Mocks.productModels.remove(product);
                productToRemove = product;
            }
        }

        return productToRemove;
    }

    @PatchMapping(APIBaseUrl + resourcePath + "/{id}")
    public ProductModel patchProduct(@PathVariable String id, @RequestBody ProductModel updateProduct){
        System.out.println("updateProduct");
        System.out.println("id: " + id);

        ProductModel productToPatch = null;

        for(ProductModel product : Mocks.productModels){
            if(product.getId().equals(id)){
                productToPatch = product;
                if(updateProduct.getDesc() != null){
                    productToPatch.setDesc(updateProduct.getDesc());
                }
                if(updateProduct.getPrice() != 0.0){
                    productToPatch.setPrice(updateProduct.getPrice());
                }

            }
        }

        return productToPatch;
    }

}
