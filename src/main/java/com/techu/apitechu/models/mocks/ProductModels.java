package com.techu.apitechu.models.mocks;

import com.techu.apitechu.models.ProductModel;

import java.util.ArrayList;

public class ProductModels {

    public static ProductModel getProduct(String id) {

        return new ProductModel(id, "Producto 1", 10);
    }

    public static ArrayList<ProductModel> getProducts() {

        ArrayList<ProductModel> productList = new ArrayList<>();

        productList.add(getProduct("1"));
        productList.add(getProduct("2"));
        productList.add(getProduct("3"));
        productList.add(getProduct("4"));

        return productList;
    }
}
