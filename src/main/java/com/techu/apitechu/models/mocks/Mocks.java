package com.techu.apitechu.models.mocks;

import com.techu.apitechu.models.ProductModel;

import java.util.ArrayList;

public class Mocks {

    public static ArrayList<ProductModel> productModels = new ProductModels().getProducts();
    public static ProductModel productModel = new ProductModels().getProduct("1");

}
